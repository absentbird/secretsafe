package main

import (
	"bufio"
	"bytes"
	"errors"
	"fmt"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"strconv"
	"strings"
)

const (
	BASEDIR = ".secretsafe"
	KEYSDIR = ".secretsafe/keys"
	SAFEDIR = ".secretsafe/secrets"
	KEYFILE = ".secretsafe/authorized_keys"
	LINKSET = ".secretsafe/linked_safes"
	LISTKEY = "SECRETSAFE_NAMES"
	PATHKEY = "SECRETSAFE_PATH_HISTORY"
)

var conf Config
var masterkey []byte
var newmaster []byte
var loud, loaded, local, lazy bool
var secrets []*Secret
var err error

func main() {
	conf.Load()
	lockMemory()
	// Split run commands from others
	runArgs := []string{}
	for i, a := range os.Args {
		if a == "run" || a == "--" {
			runArgs = os.Args[i:]
			os.Args = os.Args[:i]
			break
		}
	}
	for i := 0; i < len(os.Args); i++ {
		a := strings.ToLower(os.Args[i])
		if a == "envline" {
			loud = false
		}
		if a == "-v" || a == "--verbose" {
			loud = true
			os.Args = append(os.Args[:i], os.Args[i+1:]...)
			i = i - 1
		}
		if a == "-l" || a == "--local" {
			local = true
			os.Args = append(os.Args[:i], os.Args[i+1:]...)
			i = i - 1
		}
		if a == "-r" || a == "--do-not-rotate" {
			lazy = true
			os.Args = append(os.Args[:i], os.Args[i+1:]...)
			i = i - 1
		}
	}
	// TODO improve argument handling. Look into Kong.
	if len(os.Args) > 1 {
		if os.Args[1] == "init" {
			setup()
			return
		}
		if os.Args[1] == "prefix" {
			fmt.Println(conf.Prefix)
			return
		}
		var place int
		var overwrite, remove bool
		for num, a := range os.Args {
			if a == "add" || a == "set" || a == "rm" || a == "remove" {
				place = num
				if a == "set" {
					overwrite = true
					tail := append([]string{"secret"}, os.Args[place+1:]...)
					os.Args = append(os.Args[:place+1], tail...)
				}
				if a == "rm" || a == "remove" {
					remove = true
				}
				break
			}
		}
		if place != 0 {
			initialize()
			if len(os.Args)-place < 2 {
				if remove {
					log.Fatal("Insufficient arguments. Remove what?")
				} else {
					log.Fatal("Insufficient arguments. Add what?")
				}
			}
			switch os.Args[place+1] {
			case "key":
				var arg string
				if len(os.Args)-place > 2 {
					arg = strings.Join(os.Args[place+2:], " ")
				}
				pubKey := getValue(arg)
				if remove {
					if loud {
						fmt.Println("Are you sure you want to remove the provided key?")
						fmt.Println(pubKey)
						fmt.Println("Y/n")
						var resp string
						fmt.Scanln(&resp)
						resp = strings.ToUpper(resp)
						if resp == "N" || resp == "NO" {
							return
						}
					}
					currentKeys, err := os.Open(KEYFILE)
					check(err, "Error reading key file '"+KEYFILE+"'")
					defer currentKeys.Close()
					scanner := bufio.NewScanner(currentKeys)
					buf := bytes.NewBuffer([]byte{})
					var n, match int
					for scanner.Scan() {
						data := scanner.Bytes()
						if string(data[:]) == pubKey {
							match = n + 1
						} else {
							buf.Write(data)
							buf.Write([]byte("\n"))
						}
						n++
					}
					check(scanner.Err(), "Error scanning keys")
					if n == 0 {
						log.Fatal("No keys found.")
					}
					if match == 0 {
						log.Fatal("Key not found:\n" + pubKey + "\n")
					}
					currentKeys.Close()
					err = os.WriteFile(KEYFILE, buf.Bytes(), 0640)
					check(err, "Error writing key file '"+KEYFILE+"'")
					if loud {
						fmt.Println("Removed key from '" + KEYFILE + "'")
					}
					encKeyFile := filepath.Join(KEYSDIR, strconv.Itoa(match))
					err = os.Remove(encKeyFile)
					check(err, "Error removing key")
					if loud {
						fmt.Println("Deleted '" + encKeyFile + "'")
					}
					return
				}
				addKey([]byte(pubKey))
			case "secret":
				if len(os.Args)-place < 3 {
					log.Fatal("Insufficient arguments. New secrets require a name and value.")
				}
				s := Secret{}
				s.Name = strings.ToUpper(os.Args[place+2])
				s.File = filepath.Join(SAFEDIR, s.Name+".safe")
				if _, err := os.Stat(s.File); !errors.Is(err, os.ErrNotExist) {
					if overwrite && loud {
						fmt.Println("Secret '" + s.Name + "' already exists.")
						fmt.Println("Are you sure you want to replace it?")
						fmt.Println("Y/n")
						var resp string
						fmt.Scanln(&resp)
						resp = strings.ToUpper(resp)
						if resp == "N" || resp == "NO" {
							return
						}
					}
					if !overwrite && !remove {
						log.Fatal("Secret '" + s.Name + "' already exists.")
					}
				} else if remove {
					log.Fatal("No file to remove.")
				}
				if remove {
					if loud {
						fmt.Println("Are you sure you want to remove secret '" + s.Name + "'?")
						fmt.Println("Y/n")
						var resp string
						fmt.Scanln(&resp)
						resp = strings.ToUpper(resp)
						if resp == "N" || resp == "NO" {
							return
						}
					}
					err = os.Remove(s.File)
					s.Unset()
					for i, secret := range secrets {
						if secret.Name == s.Name {
							secrets = append(secrets[:i], secrets[i+1:]...)
							break
						}
					}
					check(err, "Error removing secret")
					if loud {
						fmt.Println("Deleted '" + s.File + "'")
					}
					return
				}
				if len(os.Args)-place < 4 {
					var secret string
					fmt.Scanln(&secret)
					s.Data = []byte(secret)
				} else {
					s.Data = []byte(os.Args[place+3])
				}
				s.Lock()
				s.Save()
				s.Set()
			case "sops-age":
				priv := Secret{}
				priv.Name = "+SOPS_AGE_KEY"
				priv.File = filepath.Join(SAFEDIR, priv.Name+".safe")
				pub := Secret{}
				pub.Name = "+SOPS_AGE_RECIPIENTS"
				pub.File = filepath.Join(SAFEDIR, pub.Name+".safe")
				var arg string
				if len(os.Args)-place > 2 {
					arg = os.Args[place+2]
				}
				value := getValue(arg)
				pub.Data, priv.Data = parseAgeKey([]byte(value))
				priv.Lock()
				priv.Save()
				priv.Set()
				pub.Lock()
				pub.Save()
				pub.Set()
			case "sops-recipient":
                pub := Secret{}
				pub.Name = "+SOPS_AGE_RECIPIENTS"
				pub.File = filepath.Join(SAFEDIR, pub.Name+".safe")
                var current []string
                if _, err = os.Stat(pub.File); !errors.Is(os.ErrNotExist, err) {
                    pub.Load()
                    current = strings.Split(string(pub.Data[:]), ",")
                }
                var arg string
				if len(os.Args)-place > 2 {
					arg = os.Args[place+2]
				}
				value := getValue(arg)
                supplied := strings.Split(value, ",")
                for _, key := range supplied {
                    if remove {
                        for i, val := range current {
                            if val == key {
                                current = append(current[:i], current[i+1:]...)
                            }
                            break
                        }
                    } else {
                        current = append(current, key)
                    }
                }
                pub.Data = []byte(strings.Join(current, ","))
				pub.Lock()
				pub.Save()
				pub.Set()
			default:
				log.Fatal("Cannot add '" + os.Args[place+1] + "'")
			}
			return
		}
		place = 0
		for num, a := range os.Args {
			if a == "show" {
				place = num
				local = true
				break
			}
		}
		if place != 0 {
			initialize()
			if len(os.Args) < place+2 {
				log.Fatal("Insufficient arguments. Show what?")
			}
			name := strings.ToUpper(os.Args[place+1])
			if strings.HasPrefix(name, conf.Prefix) {
				name = name[len(conf.Prefix):]
			}
			if strings.HasSuffix(name, ".safe") {
				name = name[:len(name)-5]
			}
			floc := filepath.Join(SAFEDIR, name+".safe")
			if _, err := os.Stat(floc); errors.Is(err, os.ErrNotExist) {
				log.Fatal("Secret not found: " + floc)
			}
			s := Secret{}
			s.File = floc
			s.Name = name
			s.Load()
			fmt.Println(string(s.Data[:]))
			return
		}
	}
	if _, err := os.Stat(BASEDIR); errors.Is(err, os.ErrNotExist) {
		if loud {
			fmt.Println("Could not find '" + BASEDIR + "' directory.")
			fmt.Println("would you like to create one here?")
			fmt.Println("(runs setup)")
			fmt.Println("Y/n")
			var resp string
			fmt.Scanln(&resp)
			resp = strings.ToUpper(resp)
			if resp == "N" || resp == "NO" {
				return
			}
			setup()
		} else {
			log.Fatal("No '" + BASEDIR + "' directory. Try running again with init or verbose mode (-v).")
		}
	} else if _, err := os.Stat(KEYFILE); errors.Is(err, os.ErrNotExist) {
		if loud {
			fmt.Println("No keys found, try running again with init")
			return
		} else {
			log.Fatal("No key file.")
		}
	} else {
		initialize()
	}
	if masterkey == nil {
		log.Fatal("No masterkey loaded, something has gone wrong.")
	} else {
		filepath.WalkDir(KEYSDIR, keyCheck)
		filepath.WalkDir(SAFEDIR, secretCheck)
	}
	if loud && loaded {
		fmt.Println("Secrets successfully loaded.")
	}
	var place int
	var nobreak bool
	for num, a := range os.Args {
		if a == "printenv" || a == "envline" {
			place = num
			if a == "envline" {
				nobreak = true
			}
			break
		}
		if a == "names" {
			names := []string{LISTKEY}
			for _, secret := range secrets {
				names = append(names, secret.EnvName())
			}
			fmt.Println(strings.Join(names, " "))
		}
	}
	if place != 0 {
		out := []string{}
		if place+2 > len(os.Args) {
			for _, secret := range secrets {
				out = append(out, secret.EnvName()+"="+string(secret.Data[:]))
			}
		} else {
			for i := place + 1; i < len(os.Args); i++ {
				name := strings.ToUpper(os.Args[i])
				if name == LISTKEY {
					names := []string{}
					for _, secret := range secrets {
						names = append(names, secret.EnvName())
					}
					out = append(out, LISTKEY+"="+strings.Join(names, " "))
					continue
				}
				if strings.HasPrefix(name, conf.Prefix) {
					name = name[len(conf.Prefix):]
				}
				for _, secret := range secrets {
					if secret.Name == name {
						out = append(out, secret.EnvName()+"="+string(secret.Data[:]))
						break
					}
				}
			}
		}
		if nobreak {
			fmt.Print(strings.Join(out, " "))
		} else {
			for _, o := range out {
				fmt.Println(o)
			}
		}
		return
	}
	if len(runArgs) > 0 {
		if len(runArgs) < 2 {
			log.Fatal("Insufficient arguments. Run what?")
		}
		initialize()
		program := runArgs[1]
		args := []string{}
		if len(runArgs) > 2 {
			args = runArgs[2:]
		}
		cmd := exec.Command(program, args...)
		cmd.Env = os.Environ()
		cmd.Stdin = os.Stdin
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr
		err = cmd.Run()
		check(err, "Error running command")
		return
	}
}
