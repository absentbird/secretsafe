package main

// TODO add scripts for other operating systems

// BASH
// TODO support for null prefix
var bashActivate string = `#!/bin/bash
list=$(secretsafe printenv);
while IFS= read -r line; do
    export "$line"
done <<< "$list"`
var bashDeactivate string = `#!/bin/bash
unset $(secretsafe names)`
// TODO write ignore for unsafe secrets
var gitIgnore string = `secrets/*
!secrets/*.safe`
