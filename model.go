package main

import (
	"bytes"
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha512"
	"crypto/x509"
	"encoding/pem"
	"errors"
	"filippo.io/age"
	"fmt"
	"golang.org/x/crypto/ssh"
	"gopkg.in/yaml.v3"
	"io"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strconv"
	"strings"
)

type Config struct {
	Cipher string `yaml:"cipher"`
	Prefix string `yaml:"prefix"`
	IdFile string `yaml:"idfile"`
}

func (s *Config) Load() {
	homeDir, err := os.UserHomeDir()
	check(err, "Error locating home directory")
	configset := []string{
		filepath.Join(SAFEDIR, "config.yaml"),
		filepath.Join(homeDir, ".config", "secretsafe"),
		filepath.Join(homeDir, ".secretsafe"),
		"secretsafe.config.yaml",
	}
	var fp string
	for _, path := range configset {
		fp = expandPath(path)
		if _, err := os.Stat(fp); !errors.Is(err, os.ErrNotExist) {
			break
		}
		// TODO allow layering of configuration files
	}
	if _, err := os.Stat(fp); errors.Is(err, os.ErrNotExist) {
		if !loud {
			log.Fatal("No configuration.")
		}
		log.Println("No configuration file found.")
		log.Println("Would you like to create one?")
		fmt.Println("Y/n")
		var resp string
		fmt.Scanln(&resp)
		resp = strings.ToUpper(resp)
		if resp == "N" || resp == "NO" {
			log.Fatal("No configuration. See instructions.")
		}
		s.Cipher = "AES-256"
		fmt.Println("What prefix would you like for environmental variables?")
		fmt.Println("(default is blank, no prefix)")
		fmt.Scanln(&resp)
		if resp == "" {
			s.Prefix = ""
		} else if resp == "*" {
			s.Prefix = ""
		} else {
			// TODO validate prefix
			s.Prefix = resp
		}
		fmt.Println("What ID (private key) would you like to encrypt your key with?")
		fmt.Println("(default is '~/.ssh/id_rsa')")
		fmt.Scanln(&resp)
		if resp == "" {
			s.IdFile = "~/.ssh/id_rsa"
		} else {
			// TODO validate key
			s.IdFile = resp
		}
		fmt.Println("Where would you like to save the file?")
		for n, loc := range configset {
			loc = expandPath(loc)
			var num string
			if n == 0 {
				num = "0 (default)"
			} else {
				num = strconv.Itoa(n)
			}
			fmt.Println(num, ":", loc)
		}
		fmt.Scanln(&resp)
		var num int
		if resp != "" {
			num, err = strconv.Atoi(resp)
			if err != nil {
				log.Fatal("Response must be a number.")
			}
		}
		if num < 0 || num > len(configset)-1 {
			log.Fatal("Number out of range.")
		}
		fp = expandPath(configset[num])
		fmt.Println("Saving config to:", fp)
		output, err := yaml.Marshal(s)
		check(err, "Error marshaling config settings")
		err = ioutil.WriteFile(fp, output, 0640)
		check(err, "Error saving config")
		s.IdFile = expandPath(s.IdFile)
		fmt.Println("Config saved.")
		return
	}
	configFile, err := ioutil.ReadFile(fp)
	check(err, "Error reading config file "+fp)
	err = yaml.Unmarshal(configFile, &s)
	check(err, "Error parsing settings")
	s.IdFile = expandPath(s.IdFile)
}

type Secret struct {
	File string
	Name string
	Data []byte
}

func (s *Secret) Save() {
	var mk []byte
	var rotate bool
	if len(newmaster) == len(masterkey) {
		mk = newmaster
		rotate = true
	} else {
		mk = masterkey
	}
	cipherText := aesEncrypt(s.Data, mk)
	err = ioutil.WriteFile(s.File, cipherText, 0600)
	check(err, "Error writing secret")
	if loud {
		if rotate {
			fmt.Println("Rotated '" + s.File + "' secret file.")
		} else {
			fmt.Println("Created '" + s.File + "' secret file.")
		}
	}
}

func (s *Secret) Load() {
	cipherText, err := ioutil.ReadFile(s.File)
	check(err, "Error reading encrypted file "+s.File)
	s.Data = aesDecrypt(cipherText, masterkey)
	if string(s.Data[len(s.Data)-1:]) == "\n" {
		s.Data = s.Data[:len(s.Data)-1]
	}
	s.Lock()
}

func (s *Secret) Set() {
	secrets = append(secrets, s)
	fn := s.EnvName()
	os.Setenv(fn, string(s.Data[:]))
	allNames := []string{}
	prefixNames := []string{}
	for _, secret := range secrets {
		allNames = append(allNames, secret.EnvName())
		if conf.Prefix != "" && !strings.HasPrefix(secret.Name, "+") {
			prefixNames = append(prefixNames, secret.EnvName())
		}
	}
	os.Setenv(LISTKEY, strings.Join(allNames, " "))
	if conf.Prefix != "" {
		os.Setenv(conf.Prefix, strings.Join(prefixNames, " "))
	}
	loaded = true
}

func (s *Secret) Unset() {
	err = os.Unsetenv(conf.Prefix + s.Name)
	check(err, "Error unsetting variable")
}

func (s *Secret) EnvName() string {
	var name string
	if strings.HasPrefix(s.Name, "+") {
		name = s.Name[1:]
	} else {
		name = conf.Prefix + s.Name
	}
	return name
}

type CryptoInterface interface {
	EqualTo([]byte) bool
	Encrypt([]byte) []byte
	Decrypt([]byte) []byte
	PubKey() []byte
}

type AgeId struct {
	Public  []byte
	private []byte
}

func (s AgeId) EqualTo(pubKey []byte) bool {
	if res := bytes.Compare(s.Public, pubKey); res == 0 {
		return true
	}
	return false
}

func (s AgeId) Decrypt(data []byte) []byte {
	ids := s.getIds()
	br := bytes.NewReader(data)
	outReader, err := age.Decrypt(br, ids...)
	check(err, "Error decrypting data")
	out, err := io.ReadAll(outReader)
	check(err, "Error reading data")
	return out
}

func (s AgeId) Encrypt(data []byte) []byte {
	recipient, err := age.ParseX25519Recipient(string(s.Public[:]))
	check(err, "Error parsing public key")
	out := &bytes.Buffer{}
	w, err := age.Encrypt(out, recipient)
	check(err, "Error encrypting file")
	in := bytes.NewBuffer(data)
	_, err = in.WriteTo(w)
	check(err, "Error writing to encrypted file")
	err = w.Close()
	check(err, "Error closing encrypted file")
	return out.Bytes()
}

func (s AgeId) getIds() []age.Identity {
	br := bytes.NewReader(s.private)
	ids, err := age.ParseIdentities(br)
	check(err, "Error getting IDs from file")
	return ids
}

func (s AgeId) PubKey() []byte {
	return s.Public
}

type SshId struct {
	Public  []byte
	private []byte
}

func (s SshId) EqualTo(pubKey []byte) bool {
	comp := SshId{Public: pubKey}
	pub := comp.bytesToPublic()
	if s.Public == nil {
		s.Public = s.PubKey()
	}
	return s.bytesToPublic().Equal(pub)
}
func (s SshId) Decrypt(data []byte) []byte {
	priv := s.bytesToPrivate()
	hash := sha512.New()
	clearData, err := rsa.DecryptOAEP(hash, rand.Reader, priv, data, nil)
	check(err, "Error decrypting data")
	return clearData
}
func (s SshId) Encrypt(data []byte) []byte {
	pub := s.bytesToPublic()
	hash := sha512.New()
	cipherData, err := rsa.EncryptOAEP(hash, rand.Reader, pub, data, nil)
	check(err, "Error encrypting data")
	return cipherData
}
func (s SshId) bytesToPublic() *rsa.PublicKey {
	parsed, _, _, _, err := ssh.ParseAuthorizedKey(s.Public)
	if err == nil {
		parsedCrypto := parsed.(ssh.CryptoPublicKey)
		pubCrypto := parsedCrypto.CryptoPublicKey()
		pub := pubCrypto.(*rsa.PublicKey)
		return pub
	} else {
		check(err, "Error parsing public key")
	}
	block, _ := pem.Decode(s.Public)
	if block == nil || block.Type != "PUBLIC KEY" {
		log.Fatal("Failed to decode PEM block containing public key")
	}
	enc := x509.IsEncryptedPEMBlock(block)
	b := block.Bytes
	if enc {
		if loud {
			log.Println("Key is encrypted pem block.")
		}
		b, err = x509.DecryptPEMBlock(block, nil)
		check(err, "Error decrypting PEM block")
	}
	ifc, err := x509.ParsePKIXPublicKey(b)
	check(err, "Error parsing public key")
	key, ok := ifc.(*rsa.PublicKey)
	if !ok {
		log.Fatal("Error rendering public key.")
	}
	return key
}

func (s SshId) bytesToPrivate() *rsa.PrivateKey {
	block, _ := pem.Decode(s.private)
	enc := x509.IsEncryptedPEMBlock(block)
	b := block.Bytes
	if enc {
		if loud {
			log.Println("Key is encrypted pem block.")
		}
		b, err = x509.DecryptPEMBlock(block, nil)
		check(err, "Error decrypting PEM block")
	}
	key, err := x509.ParsePKCS1PrivateKey(b)
	check(err, "Error parsing private key")
	return key
}

func (s SshId) PubKey() []byte {
	if s.Public == nil {
		priv := s.bytesToPrivate()
		pub, err := ssh.NewPublicKey(&priv.PublicKey)
		check(err, "Error parsing public key")
		return ssh.MarshalAuthorizedKey(pub)
	} else {
		return s.Public
	}
}
