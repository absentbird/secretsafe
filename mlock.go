// +build !windows

package main

import (
	"syscall"
)

func lockMemory() {
	err = syscall.Mlock(masterkey)
	check(err, "Error locking memory for masterkey")
	err = syscall.Mlock(newmaster)
	check(err, "Error locking memory for new master key")
}

func (s *Secret) Lock() {
	err = syscall.Mlock(s.Data)
	check(err, "Error locking memory for secrets")
}
