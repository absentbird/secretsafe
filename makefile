BINARY_NAME=secretsafe

all: build deploy clean

build:
	GOARCH=amd64 GOOS=linux go build -o ${BINARY_NAME}-linux-amd64
	GOARCH=amd64 GOOS=darwin go build -o ${BINARY_NAME}-macos-amd64
	GOARCH=amd64 GOOS=windows go build -o ${BINARY_NAME}-windows-amd64
	GOARCH=arm64 GOOS=linux go build -o ${BINARY_NAME}-linux-arm64
	GOARCH=arm64 GOOS=darwin go build -o ${BINARY_NAME}-macos-arm64

deploy:
	cp ${BINARY_NAME}-linux-amd64 dist/amd64/linux/${BINARY_NAME}
	cp ${BINARY_NAME}-macos-amd64 dist/amd64/macos/${BINARY_NAME}
	cp ${BINARY_NAME}-windows-amd64 dist/amd64/windows/${BINARY_NAME}
	cp ${BINARY_NAME}-linux-arm64 dist/arm64/linux/${BINARY_NAME}
	cp ${BINARY_NAME}-macos-arm64 dist/arm64/macos/${BINARY_NAME}

clean:
	go clean
	rm ${BINARY_NAME}-linux-amd64
	rm ${BINARY_NAME}-macos-amd64
	rm ${BINARY_NAME}-windows-amd64
	rm ${BINARY_NAME}-linux-arm64
	rm ${BINARY_NAME}-macos-arm64
