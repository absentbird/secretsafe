package main

import (
	"bufio"
	"bytes"
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"errors"
	"filippo.io/age"
	"fmt"
	"io"
	"io/fs"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"regexp"
	"strconv"
	"strings"
)

func check(e error, m string) {
	if e != nil {
		log.Fatal(m+": ", e.Error())
	}
}

func expandPath(path string) string {
	if strings.HasPrefix(path, "~/") {
		dirname, err := os.UserHomeDir()
		check(err, "Error expanding home directory")
		path = filepath.Join(dirname, path[2:])
	} else if !strings.HasPrefix(path, "/") {
		path, _ = filepath.Abs(path)
	}
	return path
}

func generateMaster() []byte {
	key := make([]byte, 32)
	_, err = rand.Read(key)
	check(err, "Error generating base encryption key.")
	return key
}

func aesEncrypt(data []byte, key []byte) (cipherText []byte) {
	block, err := aes.NewCipher(key)
	check(err, "Error creating cipher")
	gcm, err := cipher.NewGCM(block)
	check(err, "Error creating GCM")
	nonce := make([]byte, gcm.NonceSize())
	_, err = io.ReadFull(rand.Reader, nonce)
	check(err, "Error reading nonce")
	cipherText = gcm.Seal(nonce, nonce, data, nil)
	return
}

func aesDecrypt(data []byte, key []byte) (clearText []byte) {
	block, err := aes.NewCipher(key)
	check(err, "Cipher error")
	gcm, err := cipher.NewGCM(block)
	check(err, "Cipher GCM error")
	nonce := data[:gcm.NonceSize()]
	data = data[gcm.NonceSize():]
	clearText, err = gcm.Open(nil, nonce, data, nil)
	check(err, "Error decrypting file")
	return
}

func parseAgeKey(keyData []byte) (public []byte, private []byte) {
	str := string(keyData[:])
	r, err := regexp.Compile("public key: age([a-zA-Z0-9]+)\n")
	check(err, "Error compiling regex")
	pub := r.FindString(str)
	r, err = regexp.Compile("AGE-SECRET-KEY-([a-zA-Z0-9]+)\n")
	check(err, "Error compiling regex")
	priv := r.FindString(str)
	public = []byte(pub[12:])
	private = []byte(priv)
	return
}

func encryptMaster(pubKey []byte, data []byte) (cipherText []byte) {
	var authId CryptoInterface
	if strings.HasPrefix(string(pubKey[:]), "age") {
		authId = AgeId{Public: pubKey}
	} else {
		authId = SshId{Public: pubKey}
	}
	// encrypt key with unique value, then salt with value
	salt := generateMaster()
	brinedMaster := aesEncrypt(data, salt)
	brinedMaster = append(brinedMaster, salt...)
	// encrypt again with public key
	cipherText = authId.Encrypt(brinedMaster)
	return
}

func getValue(arg string) (value string) {
	if arg == "" {
		scanner := bufio.NewScanner(os.Stdin)
		for scanner.Scan() {
			value += scanner.Text()
		}
		check(scanner.Err(), "Error scanning value from standard input")
	} else if _, err = os.Stat(arg); os.IsNotExist(err) {
		value = arg
	} else {
		b, err := ioutil.ReadFile(arg)
		check(err, "Error reading file: "+arg)
		value = string(b[:])
	}
	return
}

func addKey(publicKey []byte) {
	cipherText := encryptMaster(publicKey, masterkey)
	currentKeys, err := os.Open(KEYFILE)
	check(err, "Error reading keyfile "+KEYFILE)
	var count int
	defer currentKeys.Close()
	scanner := bufio.NewScanner(currentKeys)
	var lastLine string
	for scanner.Scan() {
		if scanner.Text() == string(publicKey[:]) {
			log.Fatal("Key already exists.")
		}
		lastLine = scanner.Text()
		count++
	}
	num := strconv.Itoa(count + 1)
	kf, err := os.OpenFile(KEYFILE, os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0600)
	check(err, "Error opening keyfile "+KEYFILE)
	defer kf.Close()
	err = ioutil.WriteFile(filepath.Join(KEYSDIR, num), cipherText, 0600)
	check(err, "Error recording key in directory")
	if loud {
		fmt.Println("Saved encrypted masterkey:", filepath.Join(KEYSDIR, num))
	}
	if lastLine != "" {
		publicKey = append([]byte("\n"), publicKey...)
	}
	_, err = kf.Write(append(publicKey, []byte("\n")...))
	check(err, "Error recording key in keyfile")
	if loud {
		fmt.Println("Recorded public key in:", KEYFILE)
	}
}

func keyChange() {
	newDir := filepath.Join(KEYSDIR, "new")
	files, err := ioutil.ReadDir(newDir)
	check(err, "Error reading directory: "+newDir)
	for _, f := range files {
		if f.IsDir() {
			continue
		}
		err = os.Rename(filepath.Join(newDir, f.Name()), filepath.Join(KEYSDIR, f.Name()))
		check(err, "Error moving file: "+f.Name())
	}
	err = os.Remove(newDir)
	check(err, "Error removing directory: "+newDir)
}

func keyCheck(path string, info fs.DirEntry, err error) error {
	check(err, "Error checking key")
	if info.IsDir() {
		return nil
	}
	_, err = strconv.Atoi(info.Name())
	if err == nil {
		return nil
	}
	newKey, err := ioutil.ReadFile(path)
	check(err, "Error reading key")
	addKey(newKey)
	err = os.Remove(path)
	check(err, "Error deleting leftovers")
	if loud {
		fmt.Println("Removed leftover file '" + path + "'")
	}
	return nil
}

func grindKey(path string, info fs.DirEntry, err error) error {
	check(err, "Error changing key")
	keyNum, err := strconv.Atoi(info.Name())
	if info.IsDir() || err != nil {
		return nil
	}
	if len(newmaster) != len(masterkey) {
		log.Fatal("New master key missing or invalid.")
	}
	currentKeys, err := os.Open(KEYFILE)
	check(err, "Error reading key file '"+KEYFILE+"'")
	defer currentKeys.Close()
	scanner := bufio.NewScanner(currentKeys)
	var n int
	var publicKey []byte
	for scanner.Scan() {
		n++
		if n != keyNum {
			continue
		}
		publicKey = scanner.Bytes()
		break
	}
	check(scanner.Err(), "Error scanning keys")
	if publicKey == nil {
		log.Fatal("Key not found: " + strconv.Itoa(keyNum))
	}
	cipherText := encryptMaster(publicKey, newmaster)
	newDir := filepath.Join(KEYSDIR, "new")
	_, err = os.Stat(newDir)
	if os.IsNotExist(err) {
		err = os.Mkdir(newDir, 0750)
		check(err, "Error creating '"+newDir+"' directory")
	}
	err = ioutil.WriteFile(filepath.Join(newDir, strconv.Itoa(keyNum)), cipherText, 0600)
	check(err, "Error recording key in directory: "+newDir)
	return nil
}

func secretCheck(path string, info fs.DirEntry, err error) error {
	check(err, "Error loading secret")
	if info.IsDir() {
		return nil
	}
	keyName := info.Name()
	if !strings.HasSuffix(keyName, ".safe") {
		s := Secret{}
		s.Name = strings.ToUpper(keyName)
		s.Data, err = ioutil.ReadFile(path)
		s.Lock()
		check(err, "Error reading new secret")
		s.File = filepath.Join(SAFEDIR, s.Name+".safe")
		s.Save()
		err = os.Remove(path)
		check(err, "Error deleting leftovers")
		if loud {
			fmt.Println("Removed leftover file '" + path + "'")
			fmt.Println("Added new secret '" + s.Name + "'")
		}
		i, err := os.Stat(s.File)
		check(err, "Error accessing new secret")
		d := fs.FileInfoToDirEntry(i)
		err = secretCheck(s.File, d, err)
		return err
	}
	keyName = keyName[:len(keyName)-5]
	s := Secret{Name: keyName, File: path}
	s.Load()
	s.Set()
	return nil
}

func changeLock(path string, info fs.DirEntry, err error) error {
	if info.IsDir() || !strings.HasSuffix(info.Name(), ".safe") {
		return nil
	}
	check(err, "Error loading secret")
	keyName := info.Name()[:len(info.Name())-5]
	s := Secret{Name: keyName, File: path}
	s.Load()
	s.Save()
	return nil
}

func fileToAuth(path string) CryptoInterface {
    var privateFile []byte
    if strings.HasPrefix(strings.ToLower(path), "sops:") {
        cmd := exec.Command("sops", "-d", path[5:])
        var output bytes.Buffer
        cmd.Stdout = &output
        err = cmd.Run()
        check(err, "Error decrypting key file.")
        privateFile = output.Bytes()
    } else {
        privateFile, err = ioutil.ReadFile(path)
        check(err, "Error opening private key ID file "+path)
    }
	var authId CryptoInterface
	br := bytes.NewReader(privateFile)
	if _, err := age.ParseIdentities(br); err == nil {
		ageId := AgeId{}
		ageId.Public, ageId.private = parseAgeKey(privateFile)
		authId = ageId
	} else {
		authId = SshId{private: privateFile}
	}
	return authId
}

func initialize() {
	authId := fileToAuth(conf.IdFile)
	currentKeys, err := os.Open(KEYFILE)
	check(err, "Error reading key file '"+KEYFILE+"'")
	defer currentKeys.Close()
	scanner := bufio.NewScanner(currentKeys)
	num := -1
	var n int
	for scanner.Scan() {
		data := scanner.Bytes()
		if authId.EqualTo(data) {
			num = n + 1
			break
		}
		n++
	}
	check(scanner.Err(), "Error scanning keys")
	if n == 0 && num == -1 {
		log.Fatal("No keys found, try running with init to perform setup.")
	}
	if num < 0 {
		log.Fatal("Could not find matching key in keys file.\nAccess Denied")
	}
	kf := filepath.Join(KEYSDIR, strconv.Itoa(num))
	encryptedkey, err := ioutil.ReadFile(kf)
	check(err, "Error reading encrypted key file "+kf)
	brinedMaster := authId.Decrypt(encryptedkey)
	check(err, "Error decrypting masterkey")
	salt := brinedMaster[len(brinedMaster)-32:]
	masterkey = aesDecrypt(brinedMaster[:len(brinedMaster)-len(salt)], salt)
	if lazy {
		return
	}
	newmaster = generateMaster()
	filepath.WalkDir(KEYSDIR, grindKey)
	filepath.WalkDir(SAFEDIR, changeLock)
	keyChange()
	masterkey = newmaster
	newmaster = []byte{}
}

func setup() {
	_, err := os.Stat(BASEDIR)
	if errors.Is(err, os.ErrNotExist) {
		err = os.Mkdir(BASEDIR, 0750)
		check(err, "Error creating '"+BASEDIR+"' directory")
		if loud {
			fmt.Println("Created", BASEDIR, "directory.")
		}
	}
	_, err = os.Stat(KEYSDIR)
	if errors.Is(err, os.ErrNotExist) {
		err = os.Mkdir(KEYSDIR, 0750)
		check(err, "Error creating keys directory")
		if loud {
			fmt.Println("Created", KEYSDIR, "directory.")
		}
	}
	if _, err = os.Stat(SAFEDIR); errors.Is(err, os.ErrNotExist) {
		err = os.Mkdir(SAFEDIR, 0700)
		check(err, "Error creating safe directory")
		if loud {
			fmt.Println("Created", SAFEDIR, "directory.")
		}
	}
	if _, err = os.Stat(filepath.Join(BASEDIR, "activate")); errors.Is(err, os.ErrNotExist) {
		ioutil.WriteFile(filepath.Join(BASEDIR, "activate"), []byte(bashActivate[:]), 0750)
		if loud {
			fmt.Println("Created activation script: ", filepath.Join(BASEDIR, "activate"))
		}
	}
	if _, err = os.Stat(filepath.Join(BASEDIR, "deactivate")); errors.Is(err, os.ErrNotExist) {
		ioutil.WriteFile(filepath.Join(BASEDIR, "deactivate"), []byte(bashDeactivate[:]), 0750)
		if loud {
			fmt.Println("Created deactivation script: ", filepath.Join(BASEDIR, "deactivate"))
		}
	}
	if _, err = os.Stat(filepath.Join(BASEDIR, ".gitignore")); errors.Is(err, os.ErrNotExist) {
		ioutil.WriteFile(filepath.Join(BASEDIR, ".gitignore"), []byte(gitIgnore[:]), 0644)
		if loud {
			fmt.Println("Created .gitignore file: ", filepath.Join(BASEDIR, ".gitignore"))
		}
	}
	if dir, _ := ioutil.ReadDir(KEYSDIR); len(dir) > 0 {
		log.Fatal("Keys directory not empty.")
	}
	_, err = os.Stat(KEYFILE)
	if errors.Is(err, os.ErrNotExist) {
		err = ioutil.WriteFile(KEYFILE, []byte{}, 0640)
		check(err, "Error creating "+KEYFILE)
	}
	if loud {
		fmt.Println("Created key file:", KEYFILE)
		fmt.Println("Generating base encryption key...")
	}
	masterkey = generateMaster()
	authId := fileToAuth(conf.IdFile)
	addKey(authId.PubKey())
}
