# SecretSafe
A simple storage solution for secrets.

## Usage
Run `secretsafe init` to perform initial setup.  
Run in verbose mode for additional messages and interactive options: `secretsafe -v`

### Adding Secrets
There are two ways to add secrets:
1. Put secret files into the `.secretsafe/secrets` directory, they will be automatically encrypted and loaded at next runtime. The resulting variable name will be prefix + filename.
2. Run `secretsafe add secret [name] [secret]`, the secret will be encrypted and saved immediately. The variable will be named prefix + name. Accepts standard input for secret. Fails if a secret with the same name already exists (use `secretsafe set` to modify secrets).

**Prefix Override:** secret names beginning with `+` will not be given a prefix.

### Adding Keys
There are two ways to add an authorized key:
1. Put public keys into the `.secretsafe/keys` directory, they will be automatically added to the `knownkeys` file and used to encrypt a corresponding decryption key at next runtime.
2. Run `secretsafe add key [public key]`, the key will be immediately added to the `knownkeys` file and used to encrypt a corresponding decryption key. Also accepts standard input or file name for public key.

**SOPS Support:** for added security, encrypt your private key with SOPS and add `sops:` to the front of the `idfile` value in your config.  
Sops must be installed and configured separately beforehand.

### Accessing Secret Variables
There are three ways to access secret variables:
1. Run commands through `secretsafe run [command]` or `secretsafe -- [command]` e.g:  
  `secretsafe -- ./start-server`
All arguments following `run` (or `--`) will be used to spawn a subprocess where all the secrets are available as environmental variables. The variable names follow the pattern of prefix (default is no prefix) + name (either the original file name, or the name entered with `secretsafe add secret`)
2. Load secrets into current shell via `source .secretsafe/activate`  
All secrets will be set as environmental variables and exported to the current user environment.  
Unset all secrets from the environment with `source .secretsafe/deactivate`  
**Warning:** Evironmental variables are usually passed to every command. Be mindful of what you run with secrets exported.
3. Access secrets via the commandline. There's several arguments to choose from:
    * `secretsafe show [name]` reveals the value of a secret. `show` only works for local secrets, it does not follow links.
    * `secretsafe printenv [[names]]` prints the provided names with their secrets in the style of `printenv`. If no names are provided all are printed.  
    * `secretsafe envline [[names]]` prints the provided names with their secrets in a single line, as preferred by `env`. If no names are provided all are printed.  
  This makes it easy to selectively include most secrets (secrets that include spaces require [extra steps](https://unix.stackexchange.com/questions/196759/setting-environment-vars-containing-space-with-env)) with `env`:  
  `env $(secretsafe envline [[names]]) [command]`

An additional environmental variable containing a list of the others will be stored at `SECRETSAFE_NAMES`. Names loaded with a prefix will also be stored at the prefix value (e.g. if secrets are loaded with a prefix of `SHH_`, the environmental variable `SHH_` would hold their names).  
This is useful for unsetting the variables at runtime, simply run `unset $[prefix]` (e.g. `unset $SECRETSAFE_NAMES`) to unset and hide all loaded secrets. 

### Additional Arguments
* `secretsafe init` runs the setup.
* `secretsafe prefix` prints the currently configured prefix.
* `secretsafe names` prints the names of all the variables that would be set.
* `secretsafe set [name] [secret]` update secret value, or create a new secret if it doesn't exist.
* `secretsafe add sops-age [age key file]` add secrets for sops based on age key file. Accepts text, standard input, or file name.
* `secretsafe add/rm sops-recipient [age public key]` add or remove recipients for sops environmental variable. Accepts text, standard input, or file name. Multiple public keys must be separated by a comma.
* `secretsafe rm key [public key]` delete key. Asks for confirmation in verbose mode. Accepts text, standard input, or file name.
* `secretsafe rm secret [name]` delete secret. Asks for confirmation in verbose mode.

### Useful Flags
* `-v`, `--verbose` verbose mode, prints more about what the system is doing.
* `-l`, `--local` do not load secrets from linked safes.
* `-r`, `--do-not-rotate` prevent keys from being rotated, saves cpu time at the expense of security.

## Configuration
Configuration is stored in yaml, files are loaded in the following order:
* `.secretsafe/config.yaml`
* `~/.config/secretsafe`
* `~/.secretsafe`
* `secretsafe.config.yaml`

### Options
The configuration options are currently fairly limited:
* **prefix**: a string prepended to each secret variable.
* **idfile**: location of private key used to verify access.
* **cipher**: not currently implemented.
