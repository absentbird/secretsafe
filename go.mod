module gitlab.com/absentbird/secretsafe

go 1.18

require (
	golang.org/x/crypto v0.0.0-20220622213112-05595931fe9d
	gopkg.in/yaml.v3 v3.0.1
)

require (
	filippo.io/age v1.0.0 // indirect
	github.com/alessio/shellescape v1.4.1 // indirect
	golang.org/x/sys v0.0.0-20220412211240-33da011f77ad // indirect
)
