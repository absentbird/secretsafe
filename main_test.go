package main

import (
	"os"
	"testing"
)

func TestExpandPath(t *testing.T) {
	got := expandPath("~/")
	want, _ := os.UserHomeDir()
	if got != want {
		t.Errorf("got %q, wanted %q", got, want)
	}
}
